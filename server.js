const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

app.set('view engine', 'ejs')

app.use(express.static('public'))

app.use('/', require('./apps/video-chat/video-chat.routes'))

io.on('connection', socket => {
  socket.on('join-room', (roomId, userId, name) => {
    socket.join(roomId)
    socket.to(roomId).broadcast.emit('user-connected', userId, name)

    socket.on('send-chat-message', message => {
      socket.to(roomId).broadcast.emit('chat-message', { message: message, userId, name })
    })

    socket.on('disconnect', () => {
      socket.to(roomId).broadcast.emit('user-disconnected', userId, name)
    })
  })
})

const PORT = process.env.PORT || 5000

server.listen(PORT)
