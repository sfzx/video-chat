const socket = io('/')
const videoGrid = document.getElementById('video-grid')
const messageContainer = document.getElementById('message-container')
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const navbarForm = document.getElementById('navbar-form')
const joinRoom = document.getElementById('join-room-id')

let room

const myPeer = new Peer(undefined, {
  host: '/',
  port: '3000'
})
const myVideo = document.createElement('video')
myVideo.muted = true
const peers = {}

let name = prompt('What is your name?')
if (!name) name = `NONAME${Math.floor(Math.random() * (1241512 - 1 + 1)) + 1}`
appendMessage('You joined')

navigator.mediaDevices.getUserMedia({
  video: true,
  audio: true
}).then(stream => {
  addVideoStream(myVideo, stream)

  myPeer.on('call', call => {
    call.answer(stream)
    const video = document.createElement('video')
    call.on('stream', userVideoStream => {
      addVideoStream(video, userVideoStream)
    })
  })

  socket.on('user-connected', (userId, name) => {
    appendMessage(`${name} connected`)
    connectNewUser(userId, stream, name)
  })
})

socket.on('user-disconnected', (userId, name) => {
  appendMessage(`${name} disconnected`)
  if (peers[userId]) {
    peers[userId].close()
  }
})

myPeer.on('open', id => {
  room = ROOM_ID
  socket.emit('join-room', ROOM_ID, id, name)
})

function connectNewUser (userId, stream, name) {
  const call = myPeer.call(userId, stream)
  const video = document.createElement('video')
  call.on('stream', userVideoStream => {
    addVideoStream(video, userVideoStream)
  })
  call.on('close', () => {
    video.remove()
  })

  peers[userId] = call
}

function addVideoStream (video, stream) {
  video.srcObject = stream
  video.addEventListener('loadedmetadata', () => {
    video.play()
  })
  videoGrid.append(video)
}

socket.on('chat-message', data => {
  appendMessage(`${data.name}: ${data.message}`)
})

messageForm.addEventListener('submit', e => {
  e.preventDefault()
  const message = messageInput.value
  appendMessage(`You: ${message}`)
  socket.emit('send-chat-message', message)
  messageInput.value = ''
})

function appendMessage (message) {
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  messageContainer.append(messageElement)
}

function copyToClipboard (e, text) {
  e.preventDefault()
  const input = document.body.appendChild(document.createElement('input'))
  input.value = text
  input.focus()
  input.select()
  document.execCommand('copy')
  input.parentNode.removeChild(input)
}

function joinRoomId (e) {
  e.preventDefault()
  location.href = document.getElementById('join-field').value
}
