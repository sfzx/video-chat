## Video chat with socket.io

# Prerequisites

Node version 12 or higher installed
Peersjs globally installed

# Installation
```
npm i
npm i -g peer
```
# Start
```
npm run start
```
# Technologies used
```
Express, socket.io, peersjs, ejs, uuid
```