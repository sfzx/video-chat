const { Router } = require('express')
const { v4: uuidV4 } = require('uuid')

const router = Router()

router.get('/', (req, res) => {
  res.redirect(`/room/${uuidV4()}`)
})

router.get('/room/:roomid', (req, res) => {
  res.render('room', { roomId: req.params.roomid })
})

module.exports = router
